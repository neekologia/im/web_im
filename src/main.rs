use futures_util::{
	stream::{SplitSink, SplitStream},
	SinkExt, StreamExt,
};
use std::{io::Write, net::SocketAddr};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio_tungstenite::{tungstenite::Message, WebSocketStream};

mod topic_broadcast;

use topic_broadcast::TopicBroadcast;

#[tokio::main]
async fn main() {
	env_logger::Builder::from_default_env()
		.format(|buf, record| {
			writeln!(
				buf,
				"{} - {}:{} - [{}] {}",
				chrono::offset::Local::now().format("%Y-%m-%dT%H:%M:%S"),
				record.file().unwrap_or("unknown"),
				record.line().unwrap_or(0),
				record.level(),
				record.args()
			)
		})
		.init();
	let listener = TcpListener::bind("0.0.0.0:14098").await.unwrap();
	let state = TopicBroadcast::default();
	loop {
		while let Ok((stream, addr)) = listener.accept().await {
			let ws_stream = tokio_tungstenite::accept_async(stream).await;
			if let Ok(ws_stream) = ws_stream {
				tokio::spawn(websocket_accept(state.clone(), ws_stream, addr));
			}
		}
	}
}

async fn websocket_accept(
	state: TopicBroadcast,
	stream: WebSocketStream<TcpStream>,
	addr: SocketAddr,
) {
	log::info!("{} connected to ws", addr.port());

	let user_id = addr.port();

	log::info!("{} spawning recv/send tasks", addr.port());

	let stream = stream.split();
	let mut ws_tx = stream.0;
	let mut ws_rx = stream.1;

	let (channel_tx, channel_rx) = channel::<String>(1);

	let send_task = send_task(state.clone(), &mut ws_tx, user_id, channel_rx);
	let recv_task = recv_task(state, &mut ws_rx, user_id, channel_tx);
	tokio::select! {
		_ = send_task => log::info!("send_task terminated"),
		_ = recv_task => log::info!("recv_task terminated"),
	}
}

async fn send_task(
	state: TopicBroadcast,
	ws_tx: &mut SplitSink<WebSocketStream<TcpStream>, Message>,
	user_id: u16,
	mut channel_name_rx: Receiver<String>,
) {
	log::info!("{} spawned send_task", user_id);
	state.create_new_topic("default".into()).await;
	let mut msg_receiver = state.subscribe(&"default".into()).await;
	loop {
		let _ = ws_tx.send(Message::Ping(vec![0u8])).await;
		log::info!("send task iteration");
		tokio::select! {
			maybe_new_channel = channel_name_rx.recv() => {
				match maybe_new_channel {
					Some(new_channel) => {
						msg_receiver = state.subscribe(&new_channel).await;
						let _ = ws_tx.send(format!("{user_id} joined {new_channel}").into()).await;
					},
					None => return
				}
			},
			maybe_result = tokio::time::timeout(std::time::Duration::from_secs(10), msg_receiver.recv()) => {
				match maybe_result {
					Ok(Ok(msg)) => {
						let _ = ws_tx.send(msg.into()).await;
					},
					Ok(Err(e)) => {
						log::error!("error while sending: {e}");
						return;
					},
					Err(_) => {
						let _ = ws_tx.send(Message::Ping(vec![])).await;
					},
				}
			}
		};
	}
}

async fn recv_task(
	state: TopicBroadcast,
	ws_rx: &mut SplitStream<WebSocketStream<TcpStream>>,
	user_id: u16,
	channel_name_sender: Sender<String>,
) {
	log::info!("{} spawned recv_task", user_id);
	let mut channel_name = "default".to_string();
	loop {
		log::info!("recv task iteration");
		match ws_rx.next().await {
			Some(Ok(Message::Text(mut text))) => {
				if text.len() > 4096 {
					text = text[..4096].to_string();
				}
				if let Some(new_channel_name) = text.strip_prefix("/") {
					state.create_new_topic(new_channel_name.to_string()).await;
					let _ = channel_name_sender.send(new_channel_name.into()).await;
					channel_name = new_channel_name.into();
				} else {
					let username = format!("[{}] ({})", channel_name, user_id);
					let msg = format!("\n{username}: {text}");

					// notify active connections
					let _ = state.publish(&channel_name, msg).await;
				}
			}
			Some(Ok(_)) => continue,
			Some(Err(e)) => {
				log::error!("error while receiving: {}", e);
				return;
			}
			None => return,
		}
	}
}
