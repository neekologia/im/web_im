use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio::sync::Mutex;

#[derive(Default, Clone)]
pub struct TopicBroadcast {
	map: Arc<Mutex<HashMap<String, Sender<String>>>>,
}

impl TopicBroadcast {
	pub async fn subscribe(&self, topic: &String) -> Receiver<String> {
		self.map.lock().await.get_mut(topic).unwrap().subscribe()
	}

	pub async fn publish(&self, topic: &String, what: String) {
		self.map.lock().await.get(topic).unwrap().send(what).unwrap();
	}

	pub async fn create_new_topic(&self, topic: String) {
		let mut map = self.map.lock().await;
		if !map.contains_key(&topic) {
			map.insert(topic, Sender::new(1));
		}
	}
}
